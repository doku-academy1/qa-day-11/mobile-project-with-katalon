#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Login feature
  I want to login with my email and password

  
  Scenario Outline: Login successfull
    Given I open application
    When I input <email> and <password> in login form
    When I click button login
    Then I close application

    Examples: 
      | email  					 |	password |
      | farhan@gmail.com |	farhan 	 |	
  
  
  Scenario Outline: Login unsuccessfull
    Given I open application
    When I input <email> and <password> in login form
    When I click button login
    When Wrong email or password message exist
    Then I close application

    Examples: 
      | email  					 |	password |
      | farhan@gmail.com |	farhan1 	 |	
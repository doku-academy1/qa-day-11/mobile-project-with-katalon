#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Register feature
  I want to create account

  
  Scenario Outline: Register successfull
    Given I open application
    When I click create account
    When I input <name>, <email>, <password>, <confirmPassword> in register form
    When I click button register
    When Register successful message exist
    Then I close application

    Examples: 
      | name | email | 					password  | confirmPassword |
      | azim | azim@gmail.com | azim |  		azim |
  
  
  Scenario Outline: Register successfull because password not matches
    Given I open application
    When I click create account
    When I input <name>, <email>, <password>, <confirmPassword> in register form
    When I click button register
    When Password not matches message
    Then I close application

    Examples: 
      | name | email | 					password  | confirmPassword |
      | azim | azim@gmail.com | azim |  		azin |
    
      
  Scenario Outline: Register unsuccessfull because already member
    Given I open application
    When I click create account
    When I input <name>, <email>, <password>, <confirmPassword> in register form
    When I click button register
    When Already member login message
    Then I close application

    Examples: 
      | name | email | 					password  | confirmPassword |
      | azim | azim@gmail.com | azim |  		azim |
      
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class registerSuccessful {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@When("I click create account")
	def I_click_create_account() {
		Mobile.callTestCase(findTestCase('pages/click button create account'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input (.*), (.*), (.*), (.*) in register form")
	def I_verify_the_status_in_step(String name, String email, String pass, String confirmPass) {
		Mobile.callTestCase(findTestCase('pages/input name, email, password, confirm password register'), [('name') : name, ('email') : email, ('password'): pass, ('confirmPassword'): confirmPass], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click button register")
	def I_click_button_register() {
		Mobile.callTestCase(findTestCase('pages/click button register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Register successful message exist")
	def Register_successful_message_exist() {
		Mobile.callTestCase(findTestCase('pages/register success message'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}